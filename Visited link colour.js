// ==UserScript==
// @name         visited links
// @namespace    http://www.-.com/
// @version      0.1
// @description  change the colour/color of a visited link.
// @author       --
// @include      *
// @match        *

// To change CSS
// @grant    GM_addStyle

// ==/UserScript==
//a:link { color: red ! important }  // if you also want to change the color of unvisited links
//a:visited { color: black ! important }

GM_addStyle ( `a:visited {color: yellow ! important;}` );